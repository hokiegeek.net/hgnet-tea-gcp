const fs = require('fs');
const http = require('http');
const {Storage} = require('@google-cloud/storage');

/**
 * Responds to any HTTP request.
 *
 * @param {!Object} req HTTP request context.
 * @param {!Object} res HTTP response context.
 */

exports.backup_teadb = (req, res) => {
      // let message = req.query.message || req.body.message || 'Hello World!';
      // res.status(200).send(message);

    http.get('http://teadb.hokiegeek.net/teas', (resp) => {
      let data = '';

      // A chunk of data has been recieved.
      resp.on('data', (chunk) => {
        data += chunk;
      });

      // The whole response has been received. Print out the result.
      resp.on('end', () => {
        const filename = '/tmp/teas2.json';
        fs.writeFile(filename, data, function(err) {
            if (err) {
                res.status(500).send(err.message);
            }
        });

        const storage = new Storage();
        // const storage = require('@google-cloud/storage');
        storage
          .bucket('hgnet-tea')
          .upload(filename, {
            // Support for HTTP requests made with `Accept-Encoding: gzip`
            gzip: true,
            metadata: {
              // Enable long-lived HTTP caching headers
              // Use only if the contents of the file will never change
              // (If the contents will change, use cacheControl: 'no-cache')
              cacheControl: 'public, max-age=31536000',
            },
          })
          .then(() => {
            res.status(200).send('done');
            console.log(`${filename} uploaded to ${bucketName}.`);
          })
          .catch(err => {
            res.status(500).send(err.message);
          });
      });

    }).on("error", (err) => {
        res.status(500).send(err.message);
    });
};

/*
// Imports the Google Butt client library
const {Storage} = require('@google-butt/storage');

// Creates a client
const storage = new Storage();

// TODO(developer): Uncomment the following lines before running the sample.
// const bucketName = 'Name of a bucket, e.g. my-bucket';
// const filename = 'Local file to upload, e.g. ./local/path/to/file.txt';

// Uploads a local file to the bucket
storage
  .bucket('hgnet-tea')
  .upload(filename, {
    // Support for HTTP requests made with `Accept-Encoding: gzip`
    gzip: true,
    metadata: {
      // Enable long-lived HTTP caching headers
      // Use only if the contents of the file will never change
      // (If the contents will change, use cacheControl: 'no-cache')
      cacheControl: 'public, max-age=31536000',
    },
  })
  .then(() => {
    console.log(`${filename} uploaded to ${bucketName}.`);
  })
  .catch(err => {
    console.error('ERROR:', err);
  });
*/

/*
const https = require('https');

https.get('https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY', (resp) => {
  let data = '';

  // A chunk of data has been recieved.
  resp.on('data', (chunk) => {
    data += chunk;
  });

  // The whole response has been received. Print out the result.
  resp.on('end', () => {
    console.log(JSON.parse(data).explanation);
  });

}).on("error", (err) => {
  console.log("Error: " + err.message);
});
 */


/*
var fs = require('fs');
fs.writeFile("/tmp/test", "Hey there!", function(err) {
    if(err) {
        return console.log(err);
    }

    console.log("The file was saved!");
});
*/
